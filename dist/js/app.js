const video = document.getElementById('inputVideo');

Promise.all([
  faceapi.nets.mtcnn.loadFromUri('/models'),
  faceapi.nets.faceRecognitionNet.loadFromUri('/models')
]).then(startVideo);

function startVideo() {
  navigator.getUserMedia(
    { video: {} },
    stream => (video.srcObject = stream),
    err => console.error(err)
  );
}

video.addEventListener('play', () => {
  const canvas = faceapi.createCanvasFromMedia(video);
  document.body.append(canvas);
  const displaySize = { width: video.width, height: video.height };
  faceapi.matchDimensions(canvas, displaySize);
  const mtcnnForwardParams = {
    // number of scaled versions of the input image passed through the CNN
    // of the first stage, lower numbers will result in lower inference time,
    // but will also be less accurate
    maxNumScales: 10,
    // scale factor used to calculate the scale steps of the image
    // pyramid used in stage 1
    scaleFactor: 0.709,
    // the score threshold values used to filter the bounding
    // boxes of stage 1, 2 and 3
    scoreThresholds: [0.6, 0.7, 0.7],
    // mininum face size to expect, the higher the faster processing will be,
    // but smaller faces won't be detected
    minFaceSize: 200
  };
  setInterval(async () => {
    const mtcnnResults = await faceapi.mtcnn(video, mtcnnForwardParams);
    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
    faceapi.drawDetection(
      'canvas',
      mtcnnResults.map(res => res.faceDetection),
      { withScore: false }
    );
    faceapi.drawLandmarks(
      'canvas',
      mtcnnResults.map(res => res.faceLandmarks),
      { lineWidth: 4, color: 'red' }
    );
  }, 100);
});
